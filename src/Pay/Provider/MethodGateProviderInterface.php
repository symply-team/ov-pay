<?php


namespace Ov\Pay\Provider;

use Ov\Pay\Entity\MethodGate;

interface MethodGateProviderInterface
{
    /**
     * @param int $methodId
     * @return MethodGate|null
     */
    public function getOneByMethod($methodId);

    /**
     * @param array|null $aAvailableGates
     * @return MethodGateProviderInterface
     */
    public function setAvailableGates(array $aAvailableGates = null);
}