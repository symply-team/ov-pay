<?php


namespace Ov\Pay\Provider;

use Ov\Pay\Entity\Gate;

interface GateProviderInterface
{
    /**
     * @param int $id
     * @return Gate|null
     */
    public function getOneById($id);

    /**
     * @param string $sid
     * @return Gate|null
     */
    public function getOneBySid($sid);

    /**
     * @param array|null $aAvailableGates
     * @return GateProviderInterface
     */
    public function setAvailableGates(array $aAvailableGates = null);
}