<?php


namespace Ov\Pay\Provider;

interface OptionsProviderInterface
{
    /**
     * @param string $gateSid
     * @return array
     */
    public function getGateOptions($gateSid);

    /**
     * @param string $methodSid
     * @return array
     */
    public function getMethodOptions($methodSid);
}