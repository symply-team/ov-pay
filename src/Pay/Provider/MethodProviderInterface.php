<?php


namespace Ov\Pay\Provider;

use Ov\Pay\Entity\Method;

interface MethodProviderInterface
{
    /**
     * @param int $id
     * @return Method|null
     */
    public function getOneById($id);

    /**
     * @param string $sid
     * @return Method|null
     */
    public function getOneBySid($sid);

    /**
     * @param array|null $aAvailableMethods
     * @return MethodProviderInterface
     */
    public function setAvailableMethods(array $aAvailableMethods = null);
}