<?php

namespace Ov\Pay\Driver;

use Ov\Pay\Callback\CallbackInterface;
use Ov\Pay\Action\PayActionInterface;

use Psr\Log\LoggerInterface;

/**
 * @version 0.0.3
 */
interface DriverInterface
{
    /**
     * @param float $amount
     * @param int|null $orderId
     * @param string|null $currencyCode
     * @param array $aParams
     * @return PayActionInterface
     */
    public function pay($amount, $orderId = null, $currencyCode = null, array $aParams = []);

    /**
     * @param string $type
     * @param array $aRequest
     * @return CallbackInterface
     */
    public function callback($type, array $aRequest);

    /**
     * @param array $aOptions
     * @return self
     */
    public function setOptions(array $aOptions);

    /**
     * @param string $sid
     * @return self
     */
    public function setSid($sid);

    /**
     * @param LoggerInterface $oLogger
     * @return self
     */
    public function setLogger(LoggerInterface $oLogger = null);
}