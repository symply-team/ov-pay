<?php

namespace Ov\Pay\Driver\Gate;

use Ov\Pay\Action\PayActionInterface;
use Ov\Pay\Action\PayForm;
use Ov\Pay\Callback\Callback;
use Ov\Pay\Callback\CallbackInterface;
use Ov\Pay\Exception\InvalidOptionException;
use Ov\Pay\Exception\PayProcessingException;
use Ov\Utils\Arrays;

/**
 * @version 0.0.1
 */
class FreeKassa extends AbstractGate {
    /**
     * @var string
     */
    protected $_url = 'http://www.free-kassa.ru/merchant/cash.php';

    /**
     * @var string
     */
    protected $_merchantId;

    /**
     * @var string
     */
    protected $_secret;

    /**
     * @var string
     */
    protected $_secret2;

    /**
     * @throws InvalidOptionException
     */
    protected function parseOptions()
    {
        $this->_merchantId = Arrays::str('merchantId', $this->_aOptions);

        $this->_secret = Arrays::str('secret', $this->_aOptions);
        $this->_secret2 = Arrays::str('secret2', $this->_aOptions);

        if(is_null($this->_merchantId)){
            throw new InvalidOptionException('Gate option: [merchantId] is not set or empty');
        }

        if(is_null($this->_secret)){
            throw new InvalidOptionException('Gate option: [secret] is not set or empty');
        }

        if(is_null($this->_secret2)){
            throw new InvalidOptionException('Gate option: [secret2] is not set or empty');
        }
    }

    /**
     * Process payment
     * @param float $amount
     * @param string $orderId
     * @param string $currencyCode
     * @param array $aParams
     * @return PayForm
     * @throws InvalidOptionException
     */
    protected function processPay($amount, $orderId = null, $currencyCode = null, array $aParams = [])
    {
        if(is_null($currencyCode) || $currencyCode !== 'RUB'){
            $factor = defined('DOLLAR_TO_RUB_FACTOR') ? DOLLAR_TO_RUB_FACTOR : 60;

            $amount *= $factor;
        }

        $aMethod = $this->getMethodOptions();

        $methodId = Arrays::int('id', $aMethod);

        if(is_null($methodId)){
            throw new InvalidOptionException(sprintf('Gate option: [methods.%s.id] is not set or empty', $this->_methodSid));
        }

        $sign = md5(implode(':', array($this->_merchantId, $amount, $this->_secret, $orderId)));

        return new PayForm($this->_url, 'get', [
            'm' => $this->_merchantId,
            'oa' => $amount,
            'o' => $orderId,
            's' => $sign,
            'i' => $methodId,

            'lang' => $this->getLocale()
        ]);
    }

    /**
     * @return string
     */
    protected function getLocale(){
        if(class_exists('\Ov\App\App')){
            return \Ov\App\App::getInst()->getUtils()->getI18n()->getLocale();
        }

        return 'en';
    }

    /**
     * @param array $aRequest
     * @return CallbackInterface
     * @throws PayProcessingException
     */
    protected function callbackNotify(array $aRequest){
        $this->getLogger()->debug('Result callback', $aRequest);

        $orderId = Arrays::get('MERCHANT_ORDER_ID', $aRequest);
        $externalId = Arrays::str('intid', $aRequest);

        $amount = Arrays::str('AMOUNT', $aRequest);
        $sign = Arrays::str('SIGN', $aRequest);

        if(is_null($orderId)) {
            return $this->getErrorCallback('Request param [MERCHANT_ORDER_ID] is empty');
        }

        if(is_null($amount)) {
            return $this->getErrorCallback('Request param [AMOUNT] is empty');
        }

        if(is_null($sign)) {
            return $this->getErrorCallback('Request param [SIGN] is empty');
        }

        $requestSign = md5($this->_merchantId.':'.$amount.':'.$this->_secret2.':'.$orderId);

        if($requestSign != $sign) {
            return $this->getErrorCallback('Request param [SIGN] is invalid', ['sign' => $sign, 'request sign' => $requestSign]);
        }

        $oCallback = new Callback(floatval($amount), intval($orderId), $externalId);

        return $oCallback->setState(Callback::STATE_SERVICE_SUCCESS)->setServiceMessage('YES');
    }

    /**
     * @param array $aRequest
     * @return Callback
     * @throws PayProcessingException
     */
    protected function callbackSuccess(array $aRequest){
        $this->getLogger()->debug( 'Success callback', $aRequest);

        $orderId = Arrays::get('MERCHANT_ORDER_ID', $aRequest);
        $externalId = Arrays::str('intid', $aRequest);

        if(is_null($orderId)) {
            return $this->getErrorCallback('Request param [MERCHANT_ORDER_ID] is empty');
        }

        $oCallback = new Callback(null, $orderId, $externalId);

        return $oCallback->setState(Callback::STATE_REDIRECT_SUCCESS);
    }

    /**
     * @param array $aRequest
     * @return Callback
     * @throws PayProcessingException
     */
    protected function callbackFail(array $aRequest){
        $this->getLogger()->debug('Error callback', $aRequest);

        $orderId = Arrays::get('MERCHANT_ORDER_ID', $aRequest);
        $externalId = Arrays::str('intid', $aRequest);

        if(is_null($orderId)) {
            return $this->getErrorCallback('Request param [MERCHANT_ORDER_ID] is empty');
        }

        $oCallback = new Callback(null, $orderId, $externalId);

        return $oCallback->setState(Callback::STATE_REDIRECT_SUCCESS);
    }
}