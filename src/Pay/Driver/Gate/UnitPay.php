<?php

namespace Ov\Pay\Driver\Gate;

use Ov\Pay\Action\PayActionInterface;
use Ov\Pay\Action\PayForm;
use Ov\Pay\Callback\Callback;
use Ov\Pay\Callback\CallbackInterface;
use Ov\Pay\Exception\InvalidOptionException;
use Ov\Pay\Exception\PayProcessingException;

use Ov\Pay\ThirdParty\UnitPay as UnitPayClient;

use Ov\Utils\Arrays;
use Ov\Utils\Env;

/**
 * @version 0.0.1
 */
class UnitPay extends AbstractGate {
    /**
     * @var string
     */
    protected $_apiUrl = 'https://unitpay.ru/api';

    /**
     * @var string
     */
    protected $_orderCurrency = 'RUB';

    /**
     * @var string
     */
    protected $_projectId;

    /**
     * @var string
     */
    protected $_publicKey;

    /**
     * @var string
     */
    protected $_secretKey;

    /**
     * @throws InvalidOptionException
     */
    protected function parseOptions()
    {
        $this->_projectId = Arrays::str('projectId', $this->_aOptions);

        $this->_publicKey = Arrays::str('publicKey', $this->_aOptions);
        $this->_secretKey = Arrays::str('secretKey', $this->_aOptions);

        if(is_null($this->_projectId)){
            throw new InvalidOptionException('Gate option: [projectId] is not set or empty');
        }

        if(is_null($this->_publicKey)){
            throw new InvalidOptionException('Gate option: [publicKey] is not set or empty');
        }

        if(is_null($this->_secretKey)){
            throw new InvalidOptionException('Gate option: [secretKey] is not set or empty');
        }
    }

    /**
     * Process payment
     * @param float $amount
     * @param string $orderId
     * @param string $currencyCode
     * @param array $aParams
     * @return PayForm
     * @throws InvalidOptionException
     * @throws PayProcessingException
     */
    protected function processPay($amount, $orderId = null, $currencyCode = null, array $aParams = [])
    {
        if(is_null($currencyCode) || $currencyCode !== 'RUB'){
            $factor = defined('DOLLAR_TO_RUB_FACTOR') ? DOLLAR_TO_RUB_FACTOR : 66;

            $amount *= $factor;
        }

        $aMethod = $this->getMethodOptions();

        $methodSid = Arrays::str('code', $aMethod);
        $methodMode = Arrays::str('mode', $aMethod, 'api');

        if(is_null($methodSid)){
            throw new InvalidOptionException(sprintf('Gate option: [methods.%s.code] is not set or empty', $this->_methodSid));
        }

        if($methodMode === 'form'){
            $redirectUrl = $this->getPayFormUrl($methodSid, $amount, $orderId, $aParams);

            //Add method id to url
            $redirectUrl = str_replace('?', '/' . $methodSid . '?', $redirectUrl);
            $redirectUrl .= '&operator=' . $methodSid;
            $redirectUrl .= '&hideMenu=true';
        }
        else{
            $redirectUrl = $this->getPayApiUrl($methodSid, $amount, $orderId, $aParams);
        }

        $this->getLogger()->debug(sprintf('Unitpay pay url: [%s]', $redirectUrl));

        return $this->getPayFormFromUrl($redirectUrl);
    }

    /**
     * @param $methodSid
     * @param $amount
     * @param null $orderId
     * @param array $aParams
     * @return mixed
     * @throws PayProcessingException
     */
    protected function getPayApiUrl($methodSid, $amount, $orderId = null, array $aParams = []){
        $ip = Arrays::str('ip', $aParams);

        if(is_null($ip)){
            $ip = Env::getCurrentIp(true);
        }

        $oUnitPay = $this->getClient();

        $aQuery = [
            'account'     => $orderId,
            'desc'        => $this->getItemName($orderId),
            'sum'         => $amount,
            'paymentType' => $methodSid,
            'ip'          => $ip,
            'currency'    => $this->_orderCurrency,
            'projectId'   => $this->_projectId,
            'secretKey'   => $this->_secretKey
        ];

        $this->getLogger()->debug('Unit pay initPayment api query', $aQuery);

        $response = $oUnitPay->api('initPayment', $aQuery);

        $this->getLogger()->debug('Unit pay initPayment api response', ['response' => $response]);

        //$paymentId = $response->result->paymentId; May be use

        if(isset($response->result->type)){
            if($response->result->type == 'redirect') {
                return $response->result->redirectUrl;
            }

            throw new PayProcessingException(sprintf('Response result type: [%s] is not support', $response->result->type));
        }

        if (isset($response->error->message)) {
            throw new PayProcessingException(sprintf('Unitpay api return error: [%s]', $response->error->message));
        }

        $this->getLogger()->error('Unitpay api response is invalid', ['apiResponse' => $response]);

        throw new PayProcessingException('Unitpay api response is invalid');
    }

    /**
     * @param $methodSid
     * @param $amount
     * @param null $orderId
     * @param array $aParams
     * @return string
     */
    protected function getPayFormUrl($methodSid, $amount, $orderId = null, array $aParams = []){
        $oUnitPay = $this->getClient();

        $redirectUrl = $oUnitPay->form(
            $this->_publicKey,
            $amount,
            $orderId,
            $this->getItemName($orderId),
            $this->_orderCurrency
        );

        return $redirectUrl;
    }



    /**
     * @return string
     */
    protected function getLocale(){
        if(class_exists('\Ov\App\App')){
            return \Ov\App\App::getInst()->getUtils()->getI18n()->getLocale();
        }

        return 'en';
    }

    /**
     * @param int|null $orderId
     * @return string
     */
    protected function getItemName($orderId = null){
        $itemName = 'Order';

        if(!is_null($orderId)) {
            $itemName .= ' #' . $orderId;
        }

        return $itemName;
    }

    /**
     * @param array $aRequest
     * @return CallbackInterface
     * @throws PayProcessingException
     */
    protected function callbackNotify(array $aRequest){
        $this->getLogger()->debug('Result callback', $aRequest);

        try {
            $oUnitPay = $this->getClient();

            // Validate request (check ip address, signature and etc)
            $oUnitPay->checkHandlerRequest();

            list($method, $aParams) = array($_GET['method'], $_GET['params']);

            if($aParams['projectId'] != $this->_projectId){
                return $this->getErrorCallback('Request param [params[projectId]] is not valid');
            }

            if($aParams['orderCurrency'] != $this->_orderCurrency){
                return $this->getErrorCallback('Request param [params[projectId]] is not valid');
            }

            if(empty($aParams['orderSum'])){
                return $this->getErrorCallback('Request param [params[orderSum]] is not set or empty');
            }

            if(empty($aParams['account'])){
                return $this->getErrorCallback('Request param [params[account]] is not set or empty');
            }

            if(empty($aParams['unitpayId'])){
                return $this->getErrorCallback('Request param [params[unitpayId]] is not set or empty');
            }

            $oCallback = new Callback(floatval($aParams['orderSum']), intval($aParams['account']), $aParams['unitpayId']);

            switch ($method) {
                // Just check order (check server status, check order in DB and etc)
                case 'check':
                    $oCallback
                        ->setState(Callback::STATE_SERVICE_RESPONSE)
                        ->setServiceMessage($oUnitPay->getSuccessHandlerResponse('Check Success. Ready to pay.'));
                    break;
                // Method Pay means that the money received
                case 'pay':
                    $oCallback
                        ->setState(Callback::STATE_SERVICE_SUCCESS)
                        ->setServiceMessage($oUnitPay->getSuccessHandlerResponse('Pay Success.'));
                    break;
                // Method Error means that an error has occurred.
                case 'error':
                    $oCallback
                        ->setState(Callback::STATE_SERVICE_ERROR)
                        ->setServiceMessage($oUnitPay->getSuccessHandlerResponse('Error logged.'));
                    break;
                // Method Refund means that the money returned to the client
                case 'refund':
                    // Please cancel the order
                    $oCallback
                        ->setState(Callback::STATE_SERVICE_ERROR)
                        ->setServiceMessage($oUnitPay->getSuccessHandlerResponse('Order canceled.'));
                    break;
            }

            return $oCallback;
        }
        catch (\Exception $e) {var_dump($e); exit();
            return $this->getErrorCallback('Exception: ' . $e->getMessage());
        }
    }

    /**
     * @param array $aRequest
     * @return Callback
     * @throws PayProcessingException
     */
    protected function callbackSuccess(array $aRequest){
        $this->getLogger()->debug( 'Success callback', $aRequest);

        $orderId = Arrays::get('account', $aRequest);

        if(is_null($orderId)) {
            return $this->getErrorCallback('Request param [account] is empty');
        }

        $oCallback = new Callback(null, $orderId);

        return $oCallback->setState(Callback::STATE_REDIRECT_SUCCESS);
    }

    /**
     * @param array $aRequest
     * @return Callback
     * @throws PayProcessingException
     */
    protected function callbackFail(array $aRequest){
        $this->getLogger()->debug('Error callback', $aRequest);

        $orderId = Arrays::get('account', $aRequest);

        if(is_null($orderId)) {
            return $this->getErrorCallback('Request param [account] is empty');
        }

        $oCallback = new Callback(null, $orderId);

        return $oCallback->setState(Callback::STATE_REDIRECT_SUCCESS);
    }

    /**
     * @return UnitPayClient\UnitPay
     */
    protected function getClient(){
        return new UnitPayClient\UnitPay($this->_secretKey);
    }

    /**
     * @param string $errorText
     * @param array $aContext
     * @return Callback
     * @throws PayProcessingException
     */
    protected function getErrorCallback($errorText, array $aContext = []){
        $this->getLogger()->error($errorText, $aContext);

        $oCallback = new Callback();

        //Convert error for unitpay response format
        $errorText = $this->getClient()->getErrorHandlerResponse($errorText);

        return $oCallback->setState(Callback::STATE_SERVICE_ERROR)->setServiceMessage($errorText);
    }
}