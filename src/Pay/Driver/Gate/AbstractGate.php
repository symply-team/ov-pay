<?php

namespace Ov\Pay\Driver\Gate;

use Ov\Pay\Driver\AbstractMethod;
use Ov\Utils\Arrays;

/**
 * @version 0.0.1
 */
abstract class AbstractGate extends AbstractMethod implements GateInterface
{
    /**
     * @var string
     */
    protected $_methodSid;

    /**
     * @param string $sid
     * @return $this
     */
    public function setMethod($sid)
    {
        $this->_methodSid = $sid;

        return $this;
    }

    /**
     * @return array
     */
    protected function getMethodOptions(){
        if(!isset($this->_aOptions['methods'])){
            return [];
        }

        return Arrays::get($this->_methodSid, $this->_aOptions['methods'], []);
    }
}