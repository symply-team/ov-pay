<?php

namespace Ov\Pay\Driver\Gate;

use Exception;
use Ov\Pay\Callback\Callback;
use Ov\Pay\Driver\AbstractMethod;

use Ov\Pay\Exception\InvalidOptionException;
use Ov\Pay\Exception\PayProcessingException;

use Ov\Pay\Action\PayForm;
use Ov\Pay\ThirdParty\Payssion\PayssionClient;

use Ov\Utils\Arrays;
use Psr\Log\LoggerInterface;

/**
 * @version 0.0.2
 */
class Payssion extends AbstractGate {
    /**
     * @var string|null
     */
    protected $_key = null;

    /**
     * @var string|null
     */
    protected $_secret = null;

    /**
     * @var bool
     */
    protected $_isLiveMode = true;

    /**
     * @var string
     */
    protected $_defaultCurrency = 'USD';

    /**
     * @param array $aRequest
     * @return Callback
     * @throws PayProcessingException
     */
    protected function callbackReturn(array $aRequest){
        $this->getLogger()->debug('Получен Payssion return callback', ['aRequest' => $aRequest]);

        $orderId = Arrays::int('order_id', $aRequest);

        if(is_null($orderId)) {
            return $this->getErrorCallback('Request param [order_id] is not set or empty', ['aRequest' => $aRequest]);
        }

        $oCallback = new Callback(null, $orderId);

        return $oCallback->setState(Callback::STATE_REDIRECT_SUCCESS);
    }

    /**
     * @param array $aRequest
     * @return Callback
     * @throws PayProcessingException
     */
    protected function callbackNotify(array $aRequest){
        $this->getLogger()->debug('Получен Payssion notify callback', ['aRequest' => $aRequest]);

        $amount = Arrays::str('amount', $aRequest);
        $orderId = Arrays::int('order_id', $aRequest);

        $notifySig = Arrays::str('notify_sig', $aRequest);
        $state = Arrays::str('state', $aRequest);

        $externalId = Arrays::str('transaction_id', $aRequest);

        $aData = array(
            'api_key' => $this->_key,
            'pm_id' => Arrays::str('pm_id', $aRequest),
            'amount' => $amount,
            'currency' => Arrays::str('currency', $aRequest),
            'order_id ' => $orderId,
            'state' => $state,
            'secret_key' => $this->_secret
        );

        if(is_null($orderId)) {
            return $this->getErrorCallback('Request param [order_id] is not set or empty');
        }

        if(is_null($notifySig)) {
            return $this->getErrorCallback('Request param [notify_sig] is not set or empty');
        }

        if(is_null($state)) {
            return $this->getErrorCallback('Request param [state] is not set or empty');
        }

        $requestSign = implode('|', array_values($aData));
        $requestSign = md5($requestSign);

        //Check notify sign
        if($requestSign != $notifySig) {
            return $this->getErrorCallback('Request param [notify_sig] is not valid', [
                'sign' => $notifySig,
                'request sign' => $requestSign
            ]);
        }

        $isComplete = ($state === 'completed');

        $oCallback = new Callback(floatval($amount), $orderId, $externalId);
        $oCallback
            ->setServiceMessage('OK')
            ->setState($isComplete ? Callback::STATE_SERVICE_SUCCESS : Callback::STATE_SERVICE_ERROR);

        return $oCallback;
    }

    /**
     * @param array $aResponse
     * @return string|null
     * @throws PayProcessingException
     */
    protected function getPayUrlFromResponse(array $aResponse){
        $todo = Arrays::str('todo', $aResponse);

        if(is_null($todo)) {
            throw new PayProcessingException('Payssion api response is invalid, param: [todo] is not found or empty');
        }

        $aTodoList = explode('|', $todo);

        if(!in_array("redirect", $aTodoList)) {
            throw new PayProcessingException('Payssion api response is invalid, param: todo[redirect] is not found or empty');
        }

        $redirectUrl = Arrays::str('redirect_url', $aResponse);

        if(is_null($redirectUrl)) {
            throw new PayProcessingException('Payssion api response is invalid, param: [redirect_url] is not found or empty');
        }

        return $redirectUrl;
    }

    /**
     * @return string
     * @throws InvalidOptionException
     */
    protected function getPmId(){
        $aMethod = $this->getMethodOptions();

        $pmId = Arrays::str('pmId', $aMethod);

        if(is_null($pmId)){
            $pmId = $this->_methodSid;
        }

        if(is_null($pmId)){
            throw new InvalidOptionException(
                sprintf('Method param: [sid] or gate option [methods.%s.pmId] is not found or empty, please check method entity or gate config.', $this->_methodSid)
            );
        }

        return $pmId;
    }

    /**
     * @throws InvalidOptionException
     */
    protected function parseOptions(){
        $this->_key = Arrays::str('key', $this->_aOptions);
        $this->_secret = Arrays::str('secret', $this->_aOptions);
        $this->_isLiveMode = Arrays::get('isLiveMode', $this->_aOptions) === true;

        if(is_null($this->_key)){
            throw new InvalidOptionException('Gate option: [key] is not set or empty');
        }

        if(is_null($this->_secret)){
            throw new InvalidOptionException('Gate option: [secret] is not set or empty');
        }
    }

    /**
     * @param float $amount
     * @param int|null $orderId
     * @param string|null $currencyCode
     * @param array $aParams
     * @return PayForm
     * @throws PayProcessingException
     * @throws InvalidOptionException
     */
    protected function processPay($amount, $orderId = null, $currencyCode = null, array $aParams = []){
        if(is_null($currencyCode)){
            $currencyCode = $this->_defaultCurrency;
        }

        $oPayssionClient = new PayssionClient($this->_key, $this->_secret, $this->_isLiveMode);
        $oPayssionClient->setSSLverify(false);

        $aResponse = null;

        $aQuery = [
            'amount' => $amount,
            'currency' => $currencyCode,
            'pm_id' => $this->getPmId(),
            'description' => '',
            'order_id' => $orderId,
        ];

        $this->getLogger()->debug('Payssion api query', $aQuery);

        try {
            $aResponse = $oPayssionClient->create($aQuery);
        }
        catch (Exception $oEx) {
            throw new PayProcessingException(
                sprintf('Payssion api throw exception: [%s]', $oEx->getMessage()) , $oEx->getCode(), $oEx
            );
        }

        if (!$oPayssionClient->isSuccess()) {
            $this->getLogger()->error('Payssion api return error', $aResponse);

            throw new PayProcessingException('Payssion api return error');
        }

        if(!is_array($aResponse)){
            $this->getLogger()->error('Payssion api response is not array', ['response' => $aResponse]);

            throw new PayProcessingException('Payssion api response is not array');
        }

        $this->getLogger()->debug('Payssion api return response', $aResponse);

        return new PayForm($this->getPayUrlFromResponse($aResponse));
    }
}