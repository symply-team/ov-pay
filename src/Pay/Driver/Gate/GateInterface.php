<?php

namespace Ov\Pay\Driver\Gate;

/**
 * @version 0.0.1
 */
interface GateInterface
{
    /**
     * @param string $sid
     * @return self
     */
    public function setMethod($sid);
}