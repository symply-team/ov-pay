<?php

namespace Ov\Pay\Driver;

use http\Params;
use Ov\Pay\Action\PayForm;
use Ov\Pay\Callback\Callback;
use Ov\Pay\Callback\CallbackInterface;
use Ov\Pay\Action\PayActionInterface;

use Ov\Pay\Exception\CallbackException;
use Ov\Pay\Exception\InvalidOptionException;
use Ov\Pay\Exception\PayProcessingException;
use Ov\Utils\Arrays;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * @version 0.0.1
 */
abstract class AbstractMethod implements DriverInterface
{
    /**
     * @var string
     */
    protected $_sid;

    /**
     * @var LoggerInterface
     */
    protected $_oLogger;

    /**
     * @var array
     */
    protected $_aOptions = [];

    /**
     * @param float $amount
     * @param string $orderId
     * @param string|null $currencyCode
     * @param array $aParams
     * @return PayActionInterface
     * @throws InvalidOptionException
     * @throws PayProcessingException
     */
    public function pay($amount, $orderId = null, $currencyCode = null, array $aParams = []){
        $this->parseOptions();

        return $this->processPay($amount, $orderId, $currencyCode, $aParams);
    }

    /**
     * @param string $type
     * @param array $aRequest
     * @return CallbackInterface
     * @throws InvalidOptionException
     * @throws CallbackException
     */
    public function callback($type, array $aRequest){
        $this->parseOptions();

        $this->getLogger()->debug('Pay callback', [
            'type' => $type,
            'request' => $aRequest
        ]);

        $methodName = 'callback' . ucwords(strtolower($type));

        if(!method_exists($this, $methodName)){
            throw new CallbackException(
                sprintf('Callback method: [%s] is not found in pay driver: [%s]', $methodName, get_class($this))
            );
        }

        $oCallback = $this->$methodName($aRequest);

        if(!$oCallback || !($oCallback instanceof CallbackInterface)){
            throw new CallbackException(
                sprintf('Callback method: [%s] of pay driver: [%s] result is not instance of CallbackInterface', $methodName, get_class($this))
            );
        }

        return $oCallback;
    }

    /**
     * @param array $aOptions
     * @return self
     */
    public function setOptions(array $aOptions){
        $this->_aOptions = $aOptions;

        return $this;
    }

    /**
     * @param string $sid
     * @return self
     */
    public function setSid($sid){
        $this->_sid = $sid;

        return $this;
    }

    /**
     * @param LoggerInterface $oLogger
     * @return self
     */
    public function setLogger(LoggerInterface $oLogger = null){
        $this->_oLogger = $oLogger;

        return $this;
    }

    /**
     * @param string $errorText
     * @param array $aContext
     * @return Callback
     * @throws PayProcessingException
     */
    protected function getErrorCallback($errorText, array $aContext = []){
        $this->getLogger()->error($errorText, $aContext);

        $oCallback = new Callback();

        return $oCallback->setState(Callback::STATE_SERVICE_ERROR)->setServiceMessage($errorText);
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger(){
        if(is_null($this->_oLogger)){
            $this->_oLogger = new NullLogger();
        }

        return $this->_oLogger;
    }

    /**
     * @throws InvalidOptionException
     */
    protected function parseOptions(){
        return;
    }

    /**
     * Check method options
     * @param float $amount
     * @param string $orderId
     * @param string $currencyCode
     * @param array $aParams
     * @throws PayProcessingException
     */
    protected abstract function processPay($amount, $orderId, $currencyCode, array $aParams = []);

    /**
     * @param string $url
     * @return PayForm
     * @throws PayProcessingException
     */
    protected function getPayFormFromUrl($url){
        $aUrl = explode('?', $url);

        $payUrl = Arrays::str(0, $aUrl);
        $payParams = Arrays::str(1, $aUrl);

        $aParams = [];

        if(empty($payUrl)){
            throw new PayProcessingException(sprintf('Form url: [%s] has invalid [url] fragment', $url));
        }

        if(empty($payParams)){
            throw new PayProcessingException(sprintf('Form url: [%s] has invalid [params] fragment', $url));
        }

        parse_str($payParams, $aParams);

        return new PayForm($payUrl, 'get', $aParams);
    }
}