<?php

namespace Ov\Pay\Entity;

/**
 * @version 0.0.1
 */
class MethodGate
{
    /**
     * @var int
     */
    public $methodId;

    /**
     * @var int
     */
    public $gateId;

    /**
     * @return array
     * @throws \Exception
     */
    public static function getFields()
    {
        return [
            'methodId'     => ['type' => 'int', 'is_required' => true],
            'gateId'    => ['type' => 'int', 'is_required' => true],

            'sid'       => ['type' => 'string', 'is_nullable' => true],
        ];
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'pay_method_gates';
    }
}