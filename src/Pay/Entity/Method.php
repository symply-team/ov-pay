<?php

namespace Ov\Pay\Entity;

/**
 * @version 0.0.2
 */
class Method
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $sid;

    /**
     * @var string|null
     */
    public $driver = null;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $image = null;

    /**
     * @var string
     */
    public $creationDt;

    /**
     * @var int
     */
    public $isActive = 1;

    /**
     * @var int
     */
    public $isDeleted = 0;

    /**
     * @return array
     * @throws \Exception
     */
    public static function getFields()
    {
        return [
            'id'           => ['type' => 'int', 'is_primary' => true],
            'sid'          => ['type' => 'string', 'is_required' => true],

            'driver'       => ['type' => 'string', 'is_nullable' => true],
            'name'         => ['type' => 'string', 'is_required' => true],
            'image'        => ['type' => 'string', 'is_nullable' => true],

            'creationDt'   => ['type' => 'string', 'default' => new \DateTime()],

            'isActive'     => ['type' => 'int', 'default' => 1],
            'isDeleted'    => ['type' => 'int', 'default' => 0]
        ];
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'pay_methods';
    }

    /**
     * @return bool
     */
    public function isActive(){
        return $this->isActive === 1;
    }

    /**
     * @return bool
     */
    public function isAvailable(){
        return $this->isActive === 1 && $this->isDeleted === 0;
    }

    /**
     * @return bool
     */
    public function isDeleted(){
        return $this->isDeleted === 1;
    }
}