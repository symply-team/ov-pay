<?php

namespace Ov\Pay\Entity;

/**
 * Payment system gateway
 * @version 0.0.3
 */
class Gate
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $sid;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $driver;

    /**
     * @var int
     */
    public $priority;

    /**
     * @var string
     */
    public $creationDt;

    /**
     * @var int
     */
    public $isActive = 1;

    /**
     * @var int
     */
    public $isDeleted = 0;

    /**
     * @return array
     */
    public static function getFields()
    {
        return [
            'id'   => ['type' => 'int', 'is_primary' => true],
            'sid' => ['type' => 'string', 'is_required' => true],
            'name' => ['type' => 'string', 'is_required' => true],
            'driver' => ['type' => 'string', 'is_required' => true],

            'priority' => ['type' => 'int', 'is_required' => true, 'default' => 0],

            'creationDt'   => ['type' => 'string', 'default' => new \DateTime()],

            'isActive'     => ['type' => 'int', 'default' => 1],
            'isDeleted'    => ['type' => 'int', 'default' => 0]
        ];
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'pay_gates';
    }

    /**
     * @return bool
     */
    public function isActive(){
        return $this->isActive === 1;
    }

    /**
     * @return bool
     */
    public function isAvailable(){
        return $this->isActive === 1 && $this->isDeleted === 0;
    }

    /**
     * @return bool
     */
    public function isDeleted(){
        return $this->isDeleted === 1;
    }
}