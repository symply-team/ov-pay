<?php

namespace Ov\Pay\Entity;

/**
 * @version 0.0.1
 */
class Currency
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $rate;

    /**
     * @return array
     */
    public static function getFields()
    {
        return [
            'id'   => ['type' => 'int', 'is_primary' => true],
            'name' => ['type' => 'string', 'is_required' => true],
            'rate' => ['type' => 'int', 'is_required' => true],
        ];
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'pay_currencies';
    }
}