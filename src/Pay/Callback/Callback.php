<?php

namespace Ov\Pay\Callback;

use Ov\Pay\Exception\PayProcessingException;

/**
 * @version 0.0.1
 */
class Callback implements CallbackInterface
{
    const STATE_SERVICE_SUCCESS = 1;
    const STATE_SERVICE_ERROR = 2;

    const STATE_REDIRECT_SUCCESS = 3;
    const STATE_REDIRECT_ERROR = 4;

    const STATE_SERVICE_RESPONSE = 5;

    /**
     * @var float
     */
    protected $_amount;

    /**
     * @var int|null
     */
    protected $_orderId;

    /**
     * @var string|null
     */
    protected $_externalId;

    /**
     * @var string|null
     */
    protected $_serviceMessage;

    /**
     * @var int
     */
    protected $_stateId;

    /**
     * @param float|null $amount
     * @param int|null $orderId
     * @param string|null $externalId
     */
    public function __construct($amount = null, $orderId = null, $externalId = null){
        $this->_amount = $amount;
        $this->_orderId = $orderId;
        $this->_externalId = $externalId;
    }

    /**
     * @return float|null
     */
    public function getAmount(){
        return $this->_amount;
    }

    /**
     * @return int|null
     */
    public function getOrderId(){
        return $this->_orderId;
    }

    /**
     * @return string|null
     */
    public function getExternalId(){
        return $this->_externalId;
    }

    /**
     * @return string
     */
    public function getServiceMessage(){
        return $this->_serviceMessage;
    }

    /**
     * @return bool
     */
    public function isService(){
        return in_array($this->_stateId, [self::STATE_SERVICE_SUCCESS, self::STATE_SERVICE_ERROR, self::STATE_SERVICE_RESPONSE]);
    }

    /**
     * @return bool
     */
    public function isSuccess(){
        return in_array($this->_stateId, [self::STATE_SERVICE_SUCCESS, self::STATE_REDIRECT_SUCCESS]);
    }

    /**
     * @return bool
     */
    public function isError(){
        return in_array($this->_stateId, [self::STATE_SERVICE_ERROR, self::STATE_REDIRECT_ERROR]);
    }

    /**
     * @return bool
     */
    public function isNotNeedProcessing(){
        return in_array($this->_stateId, [self::STATE_SERVICE_RESPONSE]);
    }

    /**
     * @param int $stateId
     * @return $this
     * @throws PayProcessingException
     */
    public function setState($stateId){
        $stateId = intval($stateId);

        if($stateId < 1 || $stateId > 5){
            throw new PayProcessingException(sprintf('State id: [%s] is invalid', $stateId));
        }

        $this->_stateId = $stateId;

        return $this;
    }

    /**
     * @param string|null $message
     * @return $this
     */
    public function setServiceMessage($message = null){
        $this->_serviceMessage = $message;

        return $this;
    }
}