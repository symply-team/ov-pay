<?php

namespace Ov\Pay\Callback;

/**
 * @version 0.0.1
 */
interface CallbackInterface
{
    /**
     * @return float|null
     */
    public function getAmount();

    /**
     * @return int|null
     */
    public function getOrderId();

    /**
     * @return string|null
     */
    public function getExternalId();

    /**
     * @return string
     */
    public function getServiceMessage();

    /**
     * @return bool
     */
    public function isService();

    /**
     * @return bool
     */
    public function isSuccess();

    /**
     * @return bool
     */
    public function isError();

    /**
     * Callback is not need transaction processing
     * @return bool
     */
    public function isNotNeedProcessing();
}