<?php


namespace Ov\Pay\App;

use Ov\App\Config\ConfigInterface;

use Ov\Pay\Provider\OptionsProviderInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * @version 0.0.1
 */
class ConfigProvider implements OptionsProviderInterface
{
    /**
     * @var string
     */
    protected $_configRoot = 'pay';

    /**
     * @var string
     */
    protected $_methodsConfigKey = 'methods';

    /**
     * @var string
     */
    protected $_gatesConfigKey = 'gates';

    /**
     * @var ConfigInterface
     */
    protected $_oConfig;

    /**
     * @var LoggerInterface
     */
    protected $_oLogger;

    public function __construct(ConfigInterface $oConfig, LoggerInterface $oLogger = null)
    {
        if(is_null($oLogger)){
            $oLogger = new NullLogger();
        }

        $this->_oConfig = $oConfig;
        $this->_oLogger = $oLogger;
    }

    /**
     * @param string $gateSid
     * @return array
     */
    public function getGateOptions($gateSid)
    {
        $key = $this->getGateConfigKey($gateSid);
        $aConfig = $this->_oConfig->get($key, []);

        if(!is_array($aConfig)){
            $this->_oLogger->warning(sprintf('Ov config param: [%s] is not array or empty', $key));
            $aConfig = [];
        }

        return $aConfig;
    }

    /**
     * @param string $methodSid
     * @return array
     */
    public function getMethodOptions($methodSid)
    {
        $key = $this->getMethodConfigKey($methodSid);
        $aConfig = $this->_oConfig->get($key, []);

        if(!is_array($aConfig)){
            $this->_oLogger->warning(sprintf('Ov config param: [%s] is not array or empty', $key));
            $aConfig = [];
        }

        return $aConfig;
    }

    /**
     * @param string $sid
     * @return string
     */
    protected function getGateConfigKey($sid)
    {
        return sprintf('%s.%s.%s', $this->_configRoot, $this->_gatesConfigKey, $sid);
    }

    /**
     * @param string $sid
     * @return string
     */
    protected function getMethodConfigKey($sid)
    {
        return sprintf('%s.%s.%s', $this->_configRoot, $this->_methodsConfigKey, $sid);
    }
}