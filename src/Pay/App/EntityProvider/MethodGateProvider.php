<?php

namespace Ov\Pay\App\EntityProvider;

use ClanCats\Hydrahon\Query\Sql\Exception;
use ClanCats\Hydrahon\Query\Sql\Select;

use Ov\App\Db\Entity\Provider\AbstractProvider;

use Ov\Pay\Entity\MethodGate;
use Ov\Pay\Provider\MethodGateProviderInterface;

/**
 * @version 0.0.1
 */
class MethodGateProvider extends AbstractProvider implements MethodGateProviderInterface
{
    /**
     * @var string
     */
    protected $_entityName = 'Ov\Pay\Entity\MethodGate';

    /**
     * @var array|null
     */
    protected $_aAvailableGates = null;

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->_entityName;
    }

    /**
     * @param int $methodId
     * @return MethodGate|null
     * @throws Exception
     * @throws \Ov\App\Exception
     */
    public function getOneByMethod($methodId)
    {
        $aEntity = $this
            ->getSelect()
            ->where('methodId', '=', intval($methodId))
            ->orderBy('priority', 'desc')
            ->one();

        return $this->getEntity($aEntity);
    }

    /**
     * @param array|null $aAvailableGates
     * @return $this
     */
    public function setAvailableGates(array $aAvailableGates = null)
    {
        $this->_aAvailableGates = $aAvailableGates;

        return $this;
    }

    /**
     * @return Select
     * @throws Exception
     * @throws \Ov\App\Exception
     */
    protected function getSelect(){
        $oSelect = $this->getTable()->select();

        if(is_array($this->_aAvailableGates)){
            $oSelect->where('gateId', 'IN', $this->_aAvailableGates);
        }

        return $oSelect;
    }
}
