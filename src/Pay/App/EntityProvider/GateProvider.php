<?php

namespace Ov\Pay\App\EntityProvider;

use ClanCats\Hydrahon\Query\Sql\Exception;
use ClanCats\Hydrahon\Query\Sql\Select;

use Ov\App\Db\Entity\Provider\AbstractProvider;

use Ov\Pay\Entity\Gate;
use Ov\Pay\Provider\GateProviderInterface;

/**
 * @version 0.0.1
 */
class GateProvider extends AbstractProvider implements GateProviderInterface
{
    /**
     * @var string
     */
    protected $_entityName = 'Ov\Pay\Entity\Gate';

    /**
     * @var array|null
     */
    protected $_aAvailableGates = null;

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->_entityName;
    }

    /**
     * @param int $id
     * @return Gate
     * @throws \ClanCats\Hydrahon\Query\Sql\Exception
     * @throws \Ov\App\Exception
     */
    public function getOneById($id){
        $aEntity = $this
            ->getSelect()
            ->where('id', '=', intval($id))
            ->one();

         return $this->getEntity($aEntity);
    }

    /**
     * @param string $sid
     * @return Gate
     * @throws \ClanCats\Hydrahon\Query\Sql\Exception
     * @throws \Ov\App\Exception
     */
    public function getOneBySid($sid){
        $aEntity = $this
            ->getSelect()
            ->where('sid', '=', strval($sid))
            ->one();

        return $this->getEntity($aEntity);
    }

    /**
     * @param array|null $aAvailableGates
     * @return $this
     */
    public function setAvailableGates(array $aAvailableGates = null)
    {
        $this->_aAvailableGates = $aAvailableGates;

        return $this;
    }

    /**
     * @return Select
     * @throws Exception
     * @throws \Ov\App\Exception
     */
    protected function getSelect(){
        $oSelect = $this->getTable()->select();

        if(is_array($this->_aAvailableGates)){
            $oSelect->where('id', 'IN', $this->_aAvailableGates);
        }

        return $oSelect;
    }
}
