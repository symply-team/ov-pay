<?php

namespace Ov\Pay\App\EntityProvider;

use ClanCats\Hydrahon\Query\Sql\Exception;
use ClanCats\Hydrahon\Query\Sql\Select;

use Ov\App\Db\Entity\Provider\AbstractProvider;

use Ov\Pay\Entity\Method;
use Ov\Pay\Provider\MethodProviderInterface;

/**
 * @version 0.0.1
 */
class MethodProvider extends AbstractProvider implements MethodProviderInterface
{
    /**
     * @var string
     */
    protected $_entityName = 'Ov\Pay\Entity\Method';

    /**
     * @var array|null
     */
    protected $_aAvailableMethods = null;

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->_entityName;
    }

    /**
     * @param int $id
     * @return Method
     * @throws \ClanCats\Hydrahon\Query\Sql\Exception
     * @throws \Ov\App\Exception
     */
    public function getOneById($id){
         $aEntity = $this
            ->getSelect()
            ->where('id', '=', intval($id))
            ->one();

         return $this->getEntity($aEntity);
    }

    /**
     * @param string $sid
     * @return Method
     * @throws \ClanCats\Hydrahon\Query\Sql\Exception
     * @throws \Ov\App\Exception
     */
    public function getOneBySid($sid){
        $aEntity = $this
            ->getSelect()
            ->where('sid', '=', strval($sid))
            ->one();

        return $this->getEntity($aEntity);
    }

    /**
     * @param array|null $aAvailableMethods
     * @return $this
     */
    public function setAvailableMethods(array $aAvailableMethods = null)
    {
        $this->_aAvailableMethods = $aAvailableMethods;

        return $this;
    }

    /**
     * @return Select
     * @throws Exception
     * @throws \Ov\App\Exception
     */
    protected function getSelect(){
        $oSelect = $this->getTable()->select();

        if(is_array($this->_aAvailableMethods)){
            $oSelect->where('id', 'IN', $this->_aAvailableMethods);
        }

        return $oSelect;
    }
}
