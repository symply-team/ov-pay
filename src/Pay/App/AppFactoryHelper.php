<?php


namespace Ov\Pay\App;

use Ov\App\ApplicationInterface;
use Ov\Pay\App\EntityProvider\GateProvider;
use Ov\Pay\App\EntityProvider\MethodGateProvider;
use Ov\Pay\App\EntityProvider\MethodProvider;
use Ov\Pay\Factory;

/**
 * @version 0.0.1
 */
class AppFactoryHelper
{
    /**
     * @param ApplicationInterface $oApp
     * @return Factory
     */
    public static function getInstance(ApplicationInterface $oApp){
        $oLogger = $oApp->getUtils()->getLogger();

        $oGates = new GateProvider($oApp);
        $oMethods = new MethodProvider($oApp);
        $oMethodGates = new MethodGateProvider($oApp);

        $oOptions = new ConfigProvider($oApp->getUtils()->getConfig(), $oLogger);

        return new Factory($oOptions, $oGates, $oMethods, $oMethodGates, $oApp->getUtils()->getCache(), $oLogger);
    }
}