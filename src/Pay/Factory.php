<?php

namespace Ov\Pay;

use Ov\Pay\Driver\DriverInterface;
use Ov\Pay\Driver\Gate\GateInterface;
use Ov\Pay\Entity\Gate;
use Ov\Pay\Entity\Method;

use Ov\Pay\Exception\DriverNotFoundException;

use Ov\Pay\Provider\OptionsProviderInterface;
use Ov\Pay\Provider\GateProviderInterface;
use Ov\Pay\Provider\MethodGateProviderInterface;
use Ov\Pay\Provider\MethodProviderInterface;

use Ov\Utils\Runtime;

use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * @version 0.0.2
 */
class Factory
{
    /**
     * @var string
     */
    protected $_driverIFace = 'Ov\Pay\Driver\DriverInterface';

    /**
     * @var string
     */
    protected $_driverNamespace = 'Ov\Pay\Driver';

    /**
     * @var int|null
     */
    protected $_lastGateId = null;

    protected $_oCachePool;

    /**
     * @var OptionsProviderInterface
     */
    protected $_oConfig;

    /**
     * @var LoggerInterface
     */
    protected $_oLogger;

    /**
     * @var GateProviderInterface
     */
    protected $_oGates;

    /**
     * @var MethodProviderInterface
     */
    protected $_oMethods;

    /**
     * @var MethodGateProviderInterface
     */
    protected $_oMethodGates;

    /**
     * Factory constructor.
     * @param OptionsProviderInterface $oConfig
     * @param GateProviderInterface $oGates
     * @param MethodProviderInterface $oMethods
     * @param MethodGateProviderInterface $oMethodGates
     * @param CacheItemPoolInterface|null $oCachePool
     * @param LoggerInterface|null $oLogger
     */
    public function __construct
    (
        OptionsProviderInterface $oConfig,
        GateProviderInterface $oGates,
        MethodProviderInterface $oMethods,
        MethodGateProviderInterface $oMethodGates,
        CacheItemPoolInterface $oCachePool = null,
        LoggerInterface $oLogger = null
    )
    {
        if (is_null($oLogger)) {
            $oLogger = new NullLogger();
        }

        $this->_oConfig = $oConfig;

        $this->_oGates = $oGates;
        $this->_oMethods = $oMethods;
        $this->_oMethodGates = $oMethodGates;

        $this->_oCachePool = $oCachePool;
        $this->_oLogger = $oLogger;
    }

    /**
     * @param int $methodId
     * @return DriverInterface
     * @throws DriverNotFoundException
     */
    public function getByMethodId($methodId)
    {
        $this->_lastGateId = null;

        $oMethod = $this->_oMethods->getOneById($methodId);

        if (is_null($oMethod)) {
            throw new DriverNotFoundException(sprintf('Method is found by id: [%s]', $methodId));
        }

        return $this->getMethodDriver($oMethod);
    }

    /**
     * @param string $methodSid
     * @return DriverInterface
     * @throws DriverNotFoundException
     */
    public function getByMethodSid($methodSid)
    {
        $this->_lastGateId = null;

        $oMethod = $this->_oMethods->getOneBySid($methodSid);

        if (is_null($oMethod)) {
            throw new DriverNotFoundException(sprintf('Method is found by token: [%s]', $methodSid));
        }

        return $this->getMethodDriver($oMethod);
    }

    /**
     * @param string $sid
     * @return DriverInterface|null
     * @throws DriverNotFoundException
     */
    public function getByCallbackSid($sid){
        $oGate = $this->_oGates->getOneBySid($sid);

        if(!is_null($oGate)){
            return $this->loadGatewayDriver($oGate);
        }

        $oMethod = $this->_oMethods->getOneBySid($sid);

        if(!is_null($oMethod)){
            return $this->loadMethodDriver($oMethod);
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function getLastGateId(){
        return $this->_lastGateId;
    }

    /**
     * @param array|null $aAvailableGates
     * @return $this
     */
    public function setAvailableGates(array $aAvailableGates = null){
        $this->_oMethodGates->setAvailableGates($aAvailableGates);
        $this->_oGates->setAvailableGates($aAvailableGates);

        return $this;
    }

    /**
     * @param array|null $aAvailableMethods
     * @return $this
     */
    public function setAvailableMethods(array $aAvailableMethods = null){
        $this->_oMethods->setAvailableMethods($aAvailableMethods);

        return $this;
    }

    /**
     * @param Method $oMethod
     * @return DriverInterface
     * @throws DriverNotFoundException
     */
    protected function getMethodDriver(Method $oMethod)
    {
        if (!$oMethod->isAvailable()) {
            throw new DriverNotFoundException(sprintf('Method: [%s] is not available', $oMethod->sid));
        }

        if (!is_null($oMethod->driver)) {
            return $this->loadMethodDriver($oMethod);
        }

        $oGate = $this->getMethodGate($oMethod);

        if (!$oGate->isAvailable()) {
            throw new DriverNotFoundException(sprintf('Method: [%s] gate: [%s] is not available', $oMethod->sid, $oGate->sid));
        }

        return $this->loadGatewayDriver($oGate, $oMethod);
    }

    /**
     * @param Method $oMethod
     * @return Gate
     * @throws DriverNotFoundException
     */
    protected function getMethodGate(Method $oMethod){
        $oMethodGate = $this->_oMethodGates->getOneByMethod($oMethod->id);

        if(is_null($oMethodGate)){
            throw new DriverNotFoundException(sprintf('Gates is not found for method: [%s]', $oMethod->sid));
        }

        $oGate = $this->_oGates->getOneById($oMethodGate->gateId);

        if(is_null($oGate)){
            throw new DriverNotFoundException(
                sprintf('Method: [%s] gate is not found by id: [%s]', $oMethodGate->gateId, $oMethod->sid)
            );
        }

        return $oGate;
    }

    /**
     * @param Method $oMethod
     * @return DriverInterface
     * @throws DriverNotFoundException
     */
    protected function loadMethodDriver(Method $oMethod){
        $driverName = $oMethod->driver;

        if (is_null($driverName)) {
            $driverName = $this->getDriverNameFromSid($oMethod->sid);
        }

        if (strpos($driverName, '\\') === false) {
            $driverName = $this->_driverNamespace . '\\' . $driverName;
        }

        $oDriver = $this->loadDriver($driverName);

        //Load method options from config provider
        $oDriver->setOptions(
            $this->_oConfig->getMethodOptions($oMethod->sid)
        );

        $oDriver
            ->setSid($oMethod->sid)
            ->setLogger($this->_oLogger);

        return $oDriver;
    }

    /**
     * Get driver class name from sid
     * @param string $sid
     * @return string
     */
    protected function getDriverNameFromSid($sid){
        $className = str_replace(['-', ' ', '_'], ['', '', ''], $sid);
        $className = strtolower($className);
        $className = ucwords($className);

        return $className;
    }

    /**
     * @param string $className
     * @return DriverInterface
     * @throws DriverNotFoundException
     */
    protected function loadDriver($className){
        if (!class_exists($className)) {;
            throw new DriverNotFoundException('Payment driver class: [' . $className . '] is not found');
        }

        if (!Runtime::isClassImplemets($className, $this->_driverIFace)) {
            throw new DriverNotFoundException(sprintf('Payment driver class: [%s] is not implements interface: [%s]', $className, $this->_driverIFace));
        }

        $oDriver = new $className();

        return $oDriver;
    }

    /**
     * @param Gate $oGate
     * @param Method|null $oMethod
     * @return DriverInterface
     * @throws DriverNotFoundException
     */
    protected function loadGatewayDriver(Gate $oGate, Method $oMethod = null){
        $driverName = $oGate->driver;

        if (strpos($driverName, '\\') === false) {
            $driverName = $this->_driverNamespace . '\Gate\\' . $driverName;
        }

        $oDriver = $this->loadDriver($driverName);

        //Load gate options from config
        $oDriver->setOptions(
            $this->_oConfig->getGateOptions($oGate->sid)
        );

        $oDriver
            ->setSid($oGate->sid)
            ->setLogger($this->_oLogger);

        if (!is_null($oMethod) && $oDriver instanceof GateInterface) {
            $oDriver->setMethod($oMethod->sid);
        }

        $this->_lastGateId = $oGate->id;

        return $oDriver;
    }
}