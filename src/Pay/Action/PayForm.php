<?php

namespace Ov\Pay\Action;


/**
 * @version 0.0.1
 */
class PayForm implements PayActionInterface
{
    /**
     * @var string
     */
    protected $_url;

    /**
     * @var string
     */
    protected $_method;

    /**
     * @var array
     */
    protected $_aData;

    /**
     * @var string
     */
    protected $_formClass;

    /**
     * @var string
     */
    protected $_formStyle = 'opacity:0; visibility:hidden;';

    /**
     * PayForm constructor.
     * @param string $url
     * @param string $method
     * @param array $aData
     */
    public function __construct($url, $method = 'get', array $aData = [])
    {
        $this->_url = htmlspecialchars($url);
        $this->_method = htmlspecialchars($method);
        $this->_aData = $aData;

        $this->_formClass = 'js-submit-' . abs(crc32($url));
    }

    /**
     * @return string
     */
    public function renderRedirect()
    {
        $html = '';
        $html .= $this->renderFormBegin();

        foreach ($this->_aData as $paramName => $paramValue){
            $html .= $this->renderParam($paramName, $paramValue);
        }

        $html .= $this->renderFormEnd();
        $html .= $this->renderSubmitScript();

        return $html;
    }

    protected function renderParam($paramName, $paramValue){
        return sprintf('<input type="hidden" name="%s" value="%s" />', htmlspecialchars($paramName), htmlspecialchars($paramValue));
    }

    protected function renderFormBegin(){
        return sprintf(
            '<form class="%s" method="GET" action="%s" style="%s"><button type="submit">Submit</button>',
            $this->_formClass,
            $this->_url,
            $this->_formStyle
        );
    }

    protected function renderFormEnd(){
        return '</form>';
    }

    protected function renderSubmitScript(){
        return sprintf('<script>document.getElementsByClassName("%s")[0].submit();</script>', $this->_formClass);
    }
}