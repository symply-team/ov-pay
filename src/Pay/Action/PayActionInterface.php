<?php

namespace Ov\Pay\Action;

/**
 * @version 0.0.1
 */
interface PayActionInterface
{
    /**
     * @return string
     */
    public function renderRedirect();
}